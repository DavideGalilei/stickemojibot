from plugins.config import api_id, api_hash, bot_token
from pyrogram import Client, filters


bot = Client(
    "stickemoji",
    api_id=api_id,
    api_hash=api_hash,
    bot_token=bot_token,
    plugins={"root": "plugins"},
)


bot.run()
