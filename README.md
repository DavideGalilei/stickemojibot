[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# Emojify Stickers Bot 
A simple telegram bot for converting stickers into fake emoji.
Demo: [@stickemojibot](https://t.me/stickemojibot)