from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from .config import BANNED_USERS, CONVERTERS
from pyrogram import Client, filters
from .strings import translations
from PIL import Image
import os


getlang = (
    lambda msg: msg.from_user.language_code
    if hasattr(translations, str(msg.from_user.language_code))
    and msg.from_user.language_code
    else "en"
)
links = {"helpbutton": b"help", "maker": "t.me/DavideGalilei"}
helpmenu = {"back": b"back"}


@Client.on_message(filters.private & filters.command("start") & ~BANNED_USERS)
def start(bot, msg):
    lang = getlang(msg)
    msg.reply(
        translations[lang][
            "help" if len(msg.command) > 1 and msg.command[1] == "help" else "start"
        ].format(MENTION=msg.from_user.mention),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(translations[lang][key], url=value)
                    if isinstance(value, str)
                    else InlineKeyboardButton(
                        translations[lang][key], callback_data=value
                    )
                ]
                for key, value in (
                    helpmenu.items()
                    if len(msg.command) > 1 and msg.command[1] == "help"
                    else links.items()
                )
            ]
        ),
    )


@Client.on_message(filters.private & filters.sticker & ~BANNED_USERS)
def convert(bot, msg):
    lang = getlang(msg)
    if msg.sticker.is_animated:
        msg.reply(translations[lang]["anim"])
        return
    if msg.chat.id in CONVERTERS:
        msg.reply(translations[lang]["queue"])
        return
    CONVERTERS.add(msg.chat.id)
    im = Image.open((path := msg.download(f"{msg.chat.id}.webp")))
    width, height = im.size
    bigside = width if width > height or width == height else height

    background = Image.new("RGBA", (bigside * 4, bigside), (0, 0, 0, 0))
    offset = (
        int(round(((bigside - width) / 2), 0)),
        int(round(((bigside - height) / 2), 0)),
    )
    background.paste(im, offset)
    background.save(path)
    msg.reply_document(
        path,
        caption="@⁣stickemojibot",
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(translations[lang]["forward"], callback_data="fw")]]
        ),
    )
    background.close()
    im.close()
    os.remove(path)
    CONVERTERS.discard(msg.chat.id)


@Client.on_callback_query()
def answer(bot, query):
    lang = getlang(query)
    if query.data == "help":
        query.message.edit(
            translations[lang]["help"],
            reply_markup=InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(translations[lang][key], url=value)
                        if isinstance(value, str)
                        else InlineKeyboardButton(
                            translations[lang][key], callback_data=value
                        )
                    ]
                    for key, value in helpmenu.items()
                ]
            ),
        )
    elif query.data == "back":
        query.message.edit(
            translations[lang]["start"].format(MENTION=query.from_user.mention),
            reply_markup=InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(translations[lang][key], url=value)
                        if isinstance(value, str)
                        else InlineKeyboardButton(
                            translations[lang][key], callback_data=value
                        )
                    ]
                    for key, value in links.items()
                ]
            ),
        )
    elif query.data == "fw":
        query.answer(translations[lang]["fwpopup"], show_alert=True)


@Client.on_inline_query()
def inline(bot, query):
    lang = getlang(query)
    query.answer(
        results=[],
        switch_pm_text=translations[lang]["helpbutton"],
        switch_pm_parameter="help",
        cache_time=300,
    )
