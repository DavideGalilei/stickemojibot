from .config import _id, maker
import platform


translations = {
    "en": {
        "maker": "Creator",
        "helpbutton": "❓ Help",
        "start": "✨ Hi {MENTION}!\n\n❔What am I supposed to do?\n🌆 I can convert your stickers in emoji.",
        "help": f"✨ <b>Send a sticker, and I'll convert it to a forwardable emoji.</b>\n\n⚠️ <i>Keep in mind that the bot uses a queue system, don't flood commands/stickers, otherwise, you could be blocked for a few seconds from using the bot.</i>\n\nMade with ❤️ by {maker} in Python {platform.python_version()} (<b><a href='https://gitlab.com/DavideGalilei/stickemojibot'>source code</a></b>).\nThanks to @licenziato for the translations.",
        "back": "👈 Go Back",
        "setlang": "Language",
        "queue": f'✋ <b>Wait few seconds before using me again!</b> Please be aware that If you encounter any problem, feel free to contact <b><a href="tg://user?id={_id}">my creator</a></b>.',
        "forward": "Forward me!",
        "fwpopup": "Forward this sticker :)",
        "anim": "😕 Animated stickers are currently unsupported.",
    },
    "it": {
        "maker": "Creatore",
        "helpbutton": "❓ Come funziona",
        "start": "✨ Ciao {MENTION}!\n\n❔Cosa faccio?\n🌆 Trasformo i tuoi sticker in emoji.",
        "help": f"✨ <b>Invia uno sticker, e lo converterò in un emoji che puoi inoltrare.</b>\n\n⚠️ <i>Tieni presente che il bot ha un sistema di coda, quindi non spammare comandi/sticker, altrimenti potresti essere bloccato per un paio di secondi dall'utilizzo del bot.</i>\n\nCreato con ❤️ da {maker} in Python {platform.python_version()} (<b><a href='https://gitlab.com/DavideGalilei/stickemojibot'>source code</a></b>).\nRingrazio @licenziato per le traduzioni.",
        "back": "👈 Indietro",
        "setlang": "Lingua",
        "queue": f'✋ <b>Aspetta un paio di secondi prima di farlo di nuovo!</b> Se ci sono problemi col bot puoi <b><a href="tg://user?id={_id}">contattarmi</a></b>.',
        "forward": "Inoltrami!",
        "fwpopup": "Inoltra questo sticker :)",
        "anim": "😕 Gli sticker animati non sono supportati per ora.",
    },
}
